/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform,TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import Torch from 'react-native-torch';
// import RNFlash from 'react-native-flash';

// Torch.switchState(true); // Turn ON
// Torch.switchState(false); // Turn OFF
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component {
  state={
    status: false
  }
  handleClick=()=>{
    if(!this.state.status)
   {
    // RNFlash.turnOnFlash();
    this.setState({
      status: true
    })
   }
   else{
    // RNFlash.turnOffFlash();
    this.setState({
      status: false
    })
   }
   Torch.switchState(this.state.status);
   
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.btn} onPress={this.handleClick } >
          <Text style={styles.txt} >Flash</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  btn: {
    backgroundColor: 'green'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  txt:{fontSize: 20,padding:10,color:'white',width:200,textAlign:'center'},
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
// https://www.npmjs.com/package/react-native-torch